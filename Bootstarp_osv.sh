#!/bin/sh
echo " 
============================================== ==========================
  Script for Cockpit VM bootstrap                creator:     
  Config files setup                                          
  Virsh networking                                            
  qemu,libvirt,ssh,lm-sensors,htop,virt-viewer   https://gitlab.com/86f81  
============================================== ========================== 
"
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi
lsb_release -a
if [ -f "/etc/apt/sources.list.d/backports.list" ]; then
  echo 'deb http://deb.debian.org/debian buster-backports main' >> /etc/apt/sources.list.d/backports.list
else 
  echo "soruces file couldn't be edited, check location"
  exit 1
fi
if  grep -q -i 'virtio' /etc/modules-load.d/modules.conf ; then
  echo "virtio module already enabled"
else 
  prinf "\nvirtio" >> /etc/modules-load.d/modules.conf
  echo "enabled virtio module, reboot after script ends" 
fi
echo "==== repo update and system upgrade, mark yes to continue if necessary ===="
  apt update && apt upgrade
clear

echo "cockpit install:"
  apt install -t buster-backports cockpit-machines pcp cockpit-pcp packagekit
  clear 
echo "additional software install:"
  apt install -y qemu-system libvirt-clients libvirt-daemon-system ssh lm-sensors htop virt-viewer
  clear
echo "start virtual networking"
  virsh net-start default && virsh net-autostart default
echo "please reboot the system"
exit 0
